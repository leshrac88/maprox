Ext.define('MA.store.Products',
{
    extend: 'Ext.data.Store',
    requires: ['MA.model.Product'],
    model: 'MA.model.Product',
    autoLoad: true,
    data:
    [
            {name: 'ASA5505-K9', price:'300.00'},
            {name: '1921-K9', price:'533.99'},
            {name: '2811', price:'870.00'},
            {name: '851-K9', price:'241.92'},
            {name: 'HWIC-4ESW', price:'187.50'},
            {name: '2821', price:'1499.00'},
            {name: '2801-SEC/K9', price:'1751.50'},
            {name: '881W-GN-A-K9', price: '597.99'},
            {name: '861-K9', price: '232.60'},
            {name: '1841', price: '232.60'},
            {name: 'HWIC-2FE', price: '507.90'},
            {name: 'ASA 5512-X', price: '2389.99'}
    ],
    lastOptions: {params: {start: 0, limit: 10}}
});