/**
 * Created with JetBrains PhpStorm.
 * User: Home
 * Date: 27.02.13
 * Time: 1:54
 * To change this template use File | Settings | File Templates.
 */
Ext.define('MA.store.ProductBuy',
{
    extend: 'Ext.data.Store',
    requires: ['MA.model.Product'],
    model: 'MA.model.Product',
    data: [],
    lastOptions: {params: {start: 0, limit: 10}}
});