Ext.define('MA.model.Product',
{
    extend: 'Ext.data.Model',
    fields:
    [
        {name: 'name', type: 'string'},
        {name: 'price', type: 'float'}
    ]
});