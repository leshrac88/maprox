Ext.define('MA.controller.Products',
{
    extend: 'Ext.app.Controller',
    requires: ['Ext.window.MessageBox'],
    stores:
    [
        'Products',
        'ProductBuy'
    ],

    models:
    [
        'Product'
    ],

    views:
    [
        'components.List',
        'components.ShopGrid'
    ],

    init: function()
    {
        this.control(
        {
            'window > button':
            {
                click: this.showSum
            }
        });
    },

    showSum: function(button)
    {
        var sum=0;
        var store = this.getStore("ProductBuy");
        localCnt = store.getCount();

        if (localCnt > 0)
        {
            for (i=0; i < localCnt; i++)
            {
                var localRecord = store.getAt(i);
                sum += localRecord.get('price');
            }
        }

        Ext.MessageBox.show(
        {
            title: 'Общая стоимость: ',
            msg: sum,
            buttons: Ext.MessageBox.OK
        });
    }
});

