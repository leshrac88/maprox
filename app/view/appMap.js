Ext.application(
    {
        requires: ['Ext.container.Viewport'],
        name: 'MA',

        appFolder: 'app',

        controllers:
            [
                'Map'
            ],

        launch: function()
        {
            Ext.create('Ext.window.Window',
            {
                layout:
                {
                    type: 'vbox',
                    align: 'center'
                },
                items:
                [
                {
                    xtype: 'openlayerspanel'
                },
                {
                    xtype: 'button',
                    //id: 'LoadBtn',
                    text: 'Load',
                    margin: 5
                }
                ]
            }).show();
        }
    });