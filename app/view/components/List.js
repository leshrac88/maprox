/**
 * Component consisting from 2 base grid component.
 */

titleText = ['OK', 'Yes'];

Ext.define('MA.view.components.List',
{
    extend: 'Ext.panel.Panel',
    alias: 'widget.userlist',
    layout:
    {
        type: 'hbox'
    },


    items:
    [
        {
            title: titleText[0],
            xtype: 'usershopgrid',
            id: 'ShopGrid',
            store: 'Products',

            viewConfig:
            {
                plugins:
                [
                    {
                        ptype: 'gridviewdragdrop',
                        dragGroup: 'firstGridDDGroup',
                        dropGroup: 'secondGridDDGroup',
                        dragText: '{0} выбранных строк{1}'
                    }
                ],
                listeners:
                {
                    drop: function(node, data, dropRec, dropPosition)
                    {
                        var dropOn = dropRec ? ' ' + dropPosition + ' ' + dropRec.get('name') : ' on empty view';

                        /**
                        * Check controls before action
                        */
                        if (!(buyGrid=Ext.getCmp("BuyGrid")))
                        {
                            alert("ERROR: no grid");
                            return;
                        }
                        if (!(buyBtn=Ext.getCmp("BuyBtn")))
                        {
                            alert("ERROR: no button");
                            return;
                        }

                        if (buyGrid.store.getCount() < 3)
                        {
                            buyBtn.setDisabled(true);
                        }
                    }
                }
            },

            bbar:
            [
                {
                    xtype: 'pagingtoolbar',
                    store: 'Products',
                    dock: 'bottom',
                    displayInfo: true
                }
            ],
            stripeRows: true
        },
        {
            title: titleText[1],
            xtype: 'usershopgrid',
            id: 'BuyGrid',
            store: 'ProductBuy',

            bbar:
            [
                {
                    xtype: 'pagingtoolbar',
                    store: 'ProductBuy',
                    dock: 'bottom',
                    displayInfo: true
                }
            ],
            viewConfig:
            {
                plugins:
                {
                    ptype: 'gridviewdragdrop',
                    dragGroup: 'secondGridDDGroup',
                    dropGroup: 'firstGridDDGroup'
                },
                listeners:
                {
                    drop: function(node, data, dropRec, dropPosition)
                    {
                        var dropOn = dropRec ? ' ' + dropPosition + ' ' + dropRec.get('name') : ' on empty view';
                        if (!(buyGrid=Ext.getCmp("BuyGrid")))
                        {
                            alert("ERROR: no grid");
                            return;
                        }
                        if (!(buyBtn=Ext.getCmp("BuyBtn")))
                        {
                            alert("ERROR: no button");
                            return;
                        }

                        if (buyGrid.store.getCount() > 2)
                        {
                            buyBtn.setDisabled(false);
                        }
                    }
                }
            },
            stripeRows: true
        }
    ],

    initComponent: function()
    {

        this.callParent(arguments);
    }
});