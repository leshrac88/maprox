/**
 * Base grid with 2 columns.
 */
Ext.define('MA.view.components.ShopGrid',
{
    extend: 'Ext.grid.Panel',
    alias: 'widget.usershopgrid',
    margin: 3,
    width: 400,
    height: 500,
    multiSelect: true,

    columns:
        [
            {
                xtype: 'gridcolumn',
                dataIndex: 'name',
                text: 'Наименование',
                flex: 1
            },
            {
                xtype: 'gridcolumn',
                dataIndex: 'price',
                text: 'Цена',
                flex: 1
            }
        ],

    initComponent: function()
    {
        this.callParent(arguments);
    }
});