/**
 * Openlayers component.
 */
var ttest;
Ext.define('MA.view.components.OpenlayersPanel',
    {
        extend: 'Ext.panel.Panel',
        alias: 'widget.openlayerspanel',
        //id: 'map',
        margin: 3,
        width: 500,
        height: 500,
        multiSelect: true,

        initComponent: function()
        {
            this.callParent(arguments);
            this.on('afterrender', this.afterRender, this);

        },

        afterRender: function()
        {
                this.map = new OpenLayers.Map(this.body.dom.id);
                this.layer = new OpenLayers.Layer.OSM('OSM Map');
                ttest = this.map;
                this.map.addControl(new OpenLayers.Control.ScaleLine());
                this.map.addControl(new OpenLayers.Control.MousePosition());


                /**
                 * create test-marker
                 * @type {OpenLayers.Projection}
                 */
                this.fromProjection = new OpenLayers.Projection("EPSG:4326");
                this.toProjection = new OpenLayers.Projection("EPSG:900913");
                this.position = new OpenLayers.LonLat(75.1, 61.15).transform(this.fromProjection, this.toProjection);
                this.zoom = 12;

                this.layer.setIsBaseLayer(true);
                this.map.addLayer(this.layer);

                /*this.vectors = new OpenLayers.Layer.Vector("Vector Layer");
                this.vectors.id = "vectorsId";
                this.point = new OpenLayers.Geometry.Point(89.1, 71.15);
                this.vectors.addFeatures([new OpenLayers.Feature.Vector(this.point)]);
                this.map.addLayer(this.vectors);
                this.vectors.redraw();

                this.point = new OpenLayers.Geometry.Collection([new OpenLayers.Geometry.Point(0,0)]);
                this.vectors.addFeatures([new OpenLayers.Feature.Vector(this.point)]);
                this.map.addLayer(this.vectors);
                this.vectors.redraw();

                this.point = new OpenLayers.Geometry.Point(5.1, 12.15);
                this.vectors.addFeatures([new OpenLayers.Feature.Vector(this.point)]);
                this.map.addLayer(this.vectors);
                this.vectors.redraw();*/


                this.markers = new OpenLayers.Layer.Markers("Markers");
                this.markers.id = "Markers";
                this.map.addLayer(this.markers);
                this.markers.addMarker(new OpenLayers.Marker(this.position));

                this.map.setCenter(this.position, this.zoom);
        }

    });