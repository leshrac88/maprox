Ext.application(
{
    requires: ['Ext.container.Viewport'],
    name: 'MA',

    appFolder: 'app',

    controllers:
    [
        'Products'
    ],

    launch: function()
    {
        Ext.create('Ext.window.Window',
        {
            layout:
            {
                type: 'vbox',
                align: 'center'
            },
            items:
            [
                {
                    xtype: 'userlist'
                },
                {
                    xtype: 'button',
                    id: 'BuyBtn',
                    disabled: true,
                    text: 'Подсчитать',
                    margin: 5
                }
            ]
        }).show();
    }
});