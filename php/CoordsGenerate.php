<?php
//namespace coordinates;
/**
 * Generate random number of markers with random Longitude/Latitude.
 * Longitude has value [-180; 180], latitude - [-90; 90].
 * For example, accuracy calculation - 6 digits after decimal point.
 * If use php 5.3 or earlier, replace [] to array()
 */

class CoordsGenerate
{
    /**
     * $numFract - number of digits after decimal point, $fractPart - fraction part of number
     * Function generate fraction part of coordinate.
     * @return str consist from 1 to 6 digits
     */
    private function createItem()
    {
        $numFract = rand(0, 6);
        $fractPart = "";
        $coord = "";

        if ($numFract == 0)
            $fractPart = "0";
        else
        {
            for ($i=0; $i < $numFract; $i++)
            {
                $fractPart .= rand(0, 9);
            }
        }

        $coord = rand(-180, 180).".".$fractPart;

        return $coord;
    }

    /**
     * $nums - number of coordinates, $a - array of coordinates
     * @return array coordinates
     */
    public function createArray()
    {
        $nums = rand(1, 10);
        $coordinates = [];

        for ($i=0; $i < $nums; $i++)
        {
            array_push($coordinates, ["longitude"=> $this->createItem(), "latitude"=> $this->createItem()]);
        }
        return $coordinates;
    }
}

$items = new CoordsGenerate;
$markers = $items->createArray();
$markers = json_encode($markers);
echo $markers;
//var_dump($markers);